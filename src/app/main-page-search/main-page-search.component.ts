import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { Component, ElementRef, ViewChild, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MatChipInputEvent } from '@angular/material/chips';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import * as _ from 'underscore';

const data = [
    {
        "Customer": "Customer 1",
        "Sector": "Retail",
        "Employer": "Tesco",
        "Job Title": "Retail Assistant",
        "Location": "Warrington WA1 2AA",
        "Salary": "�9 an hour",
        "Job Description": "Cashier duties, stock rotation, Serve customers in a friendly, professional and efficient manner.  Seeking to maximise the sales and profitability of their store. Employer is coming into the office to offer face to face interviews and further insight.  ",
        "Skills Needed": "None",
        "How to Apply": "Please ask your work coach for further information."
    },
    {
        "Customer": "Customer 1",
        "Sector": "Retail",
        "Employer": "Shop for Success",
        "Job Title": "Retail Assistant",
        "Location": "Warrington WN4 6QY",
        "Salary": "�10 an hour",
        "Job Description": "A new retail store has opened in Warrington and the employer is looking to fill 10 customer service/retail positions.  The employer has offered to deliver a group information session on Wednesday 14/7/21 to share the benefits of joining, �Team Shop for Success�.\r\nThe role will include supporting customers with general enquiries, rotating and replenishing stock, Processing payments, giving customers a positive impression of yourself and Shop for Success.\r\n",
        "Skills Needed": "None",
        "How to Apply": "To learn more about this opportunity, please contact your work coach.\r\n\r\n"
    },
    {
        "Customer": "Customer 1",
        "Sector": "Retail",
        "Employer": "BKL",
        "Job Title": "Customer Service Manager",
        "Location": "Warrington WA1 1BB",
        "Salary": "NMW",
        "Job Description": "Manager required to oversee the day to day running of the store. Shop opening required. Must be flexible",
        "Skills Needed": "Previous management experience",
        "How to Apply": "To learn more about this opportunity, please contact your work coach."
    },
    {
        "Customer": "Customer 2",
        "Sector": "Hospitality",
        "Employer": "Let's Get Fit",
        "Job Title": "Gym instructor",
        "Location": "Warrington WA5 1HH",
        "Salary": "NMW",
        "Job Description": "Be polite and friendly , demonstrate the correct way to use equipment, monitor the misuse of equipment, ensure the gym is clean, deliver exercise classes.",
        "Skills Needed": "Level 2 & 3 PT qualification",
        "How to Apply": "To learn more about this opportunity, please contact your work coach."
    },
    {
        "Customer": "Customer 2",
        "Sector": "Hospitality",
        "Employer": "Red Lion",
        "Job Title": "Bar Staff",
        "Location": "Warrington WA4 6NJ",
        "Salary": "NMW",
        "Job Description": "Greet customers and create a welcoming atmosphere, serve drinks at the bar or table, handle cash.  Employer will be interviewing in the Jobcentre on Fri 16/7/21. ",
        "Skills Needed": "Good communication skills",
        "How to Apply": "To register your interest, please contact your work coach."
    },
    {
        "Customer": "Customer 2",
        "Sector": "Hospitality",
        "Employer": "Red Lion",
        "Job Title": "Waiting Staff",
        "Location": "Warrington WA4 6NJ",
        "Salary": "NMW",
        "Job Description": "Greet customers and create a welcoming atmosphere, take food orders, ensure a clean and safe environment. ",
        "Skills Needed": "Good communication skills",
        "How to Apply": "To register your interest, please contact your work coach."
    },
    {
        "Customer": "Customer 3",
        "Sector": "Production",
        "Employer": "Stich in Time",
        "Job Title": "Embroidery and print Production",
        "Location": "Warrington WA4  6JN",
        "Salary": "�9.50 an hour",
        "Job Description": "Stitch in Time are an independent print and embroidery business. Will be operating embroidery machinery, print cutters and processing bespoke orders",
        "Skills Needed": "Knowledge and experience of print and cutting machinery",
        "How to Apply": "Email printers@print.com"
    },
    {
        "Customer": "Customer 3 ",
        "Sector": "Production",
        "Employer": "Fresh foods Ltd",
        "Job Title": "Production Operative",
        "Location": "Warrington WA3 3AP",
        "Salary": "�9.19 an hour",
        "Job Description": "General Production operative duties.  Maintaining high standards and product quality at all times.  Weighing and packaging products.  Employer has agreed to interview on site.  To learn more please contact your work coach",
        "Skills Needed": "None",
        "How to Apply": "To register your interest, please contact your work coach."
    },
    {
        "Customer": "Customer 3",
        "Sector": "Production",
        "Employer": "Go Go Go",
        "Job Title": "Productio Line Operative",
        "Location": "Warrington WA3 3AP",
        "Salary": "�9.74 and hour",
        "Job Description": "The role involves assembling kitchen cabinets on a state of the art production line.  The employer is offering a site tour to help provide further insight into the roles.  For more information please contact your work coach",
        "Skills Needed": "None",
        "How to Apply": "To register your interest or to book onto the site tour please contact your work coach."
    },
    {
        "Customer": "Customer 4",
        "Sector": "Admin",
        "Employer": "JG Ltd",
        "Job Title": "Admin Assistant",
        "Location": "Warrington WA1 2BL",
        "Salary": "�11 an hour",
        "Job Description": "Updating and invoicing sales, booking meetings, answering telephone calls, filing, photocopying",
        "Skills Needed": "Good communication skills",
        "How to Apply": "To register your interest, please contact your work coach."
    },
    {
        "Customer": "Customer 4",
        "Sector": "Admin",
        "Employer": "DWP",
        "Job Title": "Business Support Officer",
        "Location": "Warrington WA1 2BL",
        "Salary": "�12 an hour",
        "Job Description": "Managing emails, arranging meetings, producing spreadsheets and statistical data",
        "Skills Needed": "Good knowledge of excel",
        "How to Apply": "To register your interest, please contact your work coach."
    },
    {
        "Customer": "Customer 4 ",
        "Sector": "Admin",
        "Employer": "PP Finances",
        "Job Title": "Admin Oficer",
        "Location": "Warrington WA5 1UH",
        "Salary": "�10 an hour",
        "Job Description": "Updating and invoicing sales, booking meetings, answering telephone calls, filing, photocopying.  Employer will be delivering a virtual presentation to explain the role on offer.",
        "Skills Needed": "Good communication skills",
        "How to Apply": "To book onto the virtual session please contact your work coach."
    },
    {
        "Customer": "Customer 5 ",
        "Sector": "Construction",
        "Employer": "Jim's Homes",
        "Job Title": "Builder",
        "Location": "Warrington WA2 9LW",
        "Salary": "�15 an hour",
        "Job Description": "Working under site foreman to help support employers design for creation of residential properties.  Working in all weathers.",
        "Skills Needed": "CSCS required.",
        "How to Apply": "Apply by telephoning J. Jones on 01925 000000."
    },
    {
        "Customer": "Customer 5 ",
        "Sector": "Construction",
        "Employer": "Jim's Homes",
        "Job Title": "Labourer",
        "Location": "Warrington WA2 9LW",
        "Salary": "�12 an hour",
        "Job Description": "To support builders with general labouring duties.",
        "Skills Needed": "CSCS required.",
        "How to Apply": "Apply by telephoning J. Jones on 01925 000000."
    },
    {
        "Customer": "Customer 5 ",
        "Sector": "Construction",
        "Employer": "Jim's Homes",
        "Job Title": "Site Electricion",
        "Location": "Warrington WA2 9LW",
        "Salary": "�15 an hour",
        "Job Description": "Provide buildings on site with energy.  You will install, inspect"
    }
];
@Component({
    selector: 'app-main-page-search',
    templateUrl: './main-page-search.component.html',
    styleUrls: ['./main-page-search.component.css']
})

export class MainPageSearchComponent implements OnInit {

    selectable = true;
    removable = true;
    separatorKeysCodes: number[] = [ENTER, COMMA];
    postCode = new FormControl();
    sectorCtrl = new FormControl();
    filteredSectors: Observable<string[]>;
    sectors: string[] = [];
    allSectors: string[] = [];

    @ViewChild('sectorInput') sectorInput: ElementRef<HTMLInputElement>;

    constructor() {
        this.filteredSectors = this.sectorCtrl.valueChanges.pipe(
            startWith(null),
            map((fruit: string | null) => fruit ? this._filter(fruit) : this.allSectors.slice()));
    }
    ngOnInit(): void {
        this.allSectors = _.unique(_.map(data, d => d['Sector']));

    }

    add(event: MatChipInputEvent): void {
        const value = (event.value || '').trim();
        if (value) {
            this.sectors.push(value);
        }
        event.chipInput!.clear();
        this.sectorCtrl.setValue(null);
    }
    remove(fruit: string): void {
        const index = this.sectors.indexOf(fruit);
        if (index >= 0) {
            this.sectors.splice(index, 1);
        }
    }

    selected(event: MatAutocompleteSelectedEvent): void {
        this.sectors.push(event.option.viewValue);
        this.sectorInput.nativeElement.value = '';
        this.sectorCtrl.setValue(null);
    }

    private _filter(value: string): string[] {
        const filterValue = value.toLowerCase();
        return this.allSectors.filter(sector => sector.toLowerCase().includes(filterValue));
    }
    get results() {
        console.log(this.postCode.value)
        let filteredResults = _.filter(data, d => _.indexOf(this.sectors, d.Sector) > -1);

        return _.filter(filteredResults, d => this.postCode.value === null || d["Location"].toLowerCase().indexOf(this.postCode.value.toLowerCase()) > -1);
    }

}
