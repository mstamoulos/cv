import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { MatInputModule } from '@angular/material/input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatCheckboxModule } from '@angular/material/checkbox';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { StoreModule } from '@ngrx/store';
import { MatStepperModule } from '@angular/material/stepper';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';
import { MatTableModule } from '@angular/material/table';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatIconModule } from '@angular/material/icon';
import { MatChipsModule } from '@angular/material/chips';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatCardModule } from '@angular/material/card';
import { MatToolbarModule } from '@angular/material/toolbar';
import {FlexLayoutModule} from "@angular/flex-layout";

import { EditorModule } from '@tinymce/tinymce-angular';
import { MainPageSearchComponent } from './main-page-search/main-page-search.component';

@NgModule({
    declarations: [
        AppComponent,
        MainPageSearchComponent,


    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        FormsModule,
        ReactiveFormsModule,
        EditorModule,
        FlexLayoutModule,
        MatInputModule,
        MatButtonModule,
        MatStepperModule,
        MatFormFieldModule,
        MatTableModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatIconModule,
        MatChipsModule,
        MatCheckboxModule,
        MatAutocompleteModule,
        MatCardModule,
        MatToolbarModule,
        StoreModule.forRoot({}, {})
    ],
    providers: [MatDatepickerModule],
    bootstrap: [AppComponent]
})
export class AppModule { }
